#include <stdio.h>
#define CUBE(x) (6.0 * x * x)

int main()
{
    float num;

    printf("Enter the length of side: ");
    scanf("%f", &num);

    printf("CUBE(%.2f) = %.2f\n", num, CUBE(num));

    return 0;

}
